#!/bin/bash
update_nginx_conf() {
    current_nginx_conf_file=$(cat /etc/nginx/current_nginx_conf)
    nginx_conf_localssd=nginx_localssd.conf
    nginx_conf_bypass=nginx_bypass.conf
    source /mnt/gcs/global_conf
    PROJECT_ID=$(curl -s "http://metadata.google.internal/computeMetadata/v1/project/project-id" -H "Metadata-Flavor: Google")

    gcloud config set project $PROJECT_ID

    nginx_type=$(curl -s 'http://metadata.google.internal/computeMetadata/v1/instance/attributes/nginx-proxy-type' -H 'Metadata-Flavor: Google')
    if [ $nginx_type = 'localssd' ]; then
        new_conf_file="$nginx_conf_localssd"
    else
        new_conf_file="$nginx_conf_bypass"
    fi
    echo "current conf file: $current_nginx_conf_file, conf file in global_conf: $new_conf_file"

    if [ "$current_nginx_conf_file" != "$new_conf_file" ]; then
        echo 'nginx localssd conf file name changed. check if new file exists'
        if [ -e /mnt/gcs/$new_conf_file ]; then
            echo "file $new_conf_file exists, check if valid"
            ln -sf /mnt/gcs/$new_conf_file /etc/nginx/nginx.conf
            check_valid=$(nginx -t 2>&1 | grep "successful")
            if [ -n "$check_valid" ]; then
                echo "file $new_conf_file valid, reload nginx"
                is_running=$(service nginx status | grep running)
                if [ "$is_running" = "" ]; then
                    echo "start nginx service because it is not running"
                    service nginx start
                fi
                service nginx reload
                echo $new_conf_file >/etc/nginx/current_nginx_conf
                gcloud logging write nginx-conf $(hostname)": nginx conf file /mnt/gcs/$new_conf_file reloaded" --project=$PROJECT_ID
            else
                ln -sf /mnt/gcs/$current_nginx_conf_file /etc/nginx/nginx.conf
                echo "file $new_conf_file NOT valid, will NOT reload nginx"
                gcloud logging write nginx-conf $(hostname)": nginx conf file /mnt/gcs/$new_conf_file NOT valid by niginx -t check, won't load this file" --project=$PROJECT_ID --severity=ERROR
            fi
        else
            echo "file /mnt/gcs/$new_conf_file NOT exists, please check key nginx_conf_localssd in /mnt/gcs/global_conf file"
            gcloud logging write nginx-conf $(hostname)": nginx conf file /mnt/gcs/$new_conf_file NOT exists, won't load this file" --project=$PROJECT_ID --severity=ERROR
        fi
    else
        echo 'nginx localssd conf file name not changed'
    fi
}

update_stackdriver_logging() {
    source /mnt/gcs/global_conf
    if [ "$stackdriver_logging" = "on" ]; then
        echo 'start google-fluentd for stackdriver logging'
        service google-fluentd start
    else
        echo 'stop google-fluentd for stackdriver logging'
        service google-fluentd stop
        #do not want to change to vm image, use it to disable syslog
        mv /etc/google-fluentd/config.d/syslog.conf /etc/google-fluentd/config.d/syslog.conf.bak
    fi
}

exec 1>>/tmp/nginx_update_$(date -d now +%Y%m%d).log 2>&1

echo "start time "$(date "+%Y-%m-%d %H:%M:%S")"*************************************************"

# bash /mnt/gcs/command2.sh

mount_result=$(df -h | grep /mnt/gcs)
if [[ -z $mount_result ]]; then
    echo 'not mounted, waiting for /mnt/gcs initialized by init_nginx_proxy.sh'
    exit
fi

# update_stackdriver_logging
update_nginx_conf

nginx_type=$(curl -s 'http://metadata.google.internal/computeMetadata/v1/instance/attributes/nginx-proxy-type' -H 'Metadata-Flavor: Google')
if [ $nginx_type = 'localssd' ]; then
    bash /mnt/gcs/update_upstream.sh
fi

echo "end time "$(date "+%Y-%m-%d %H:%M:%S")"***************************************************"

echo ''