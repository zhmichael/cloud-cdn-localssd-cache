#!/bin/bash

init_local_ssd() {

    if [ -b "/dev/md0" ]; then
        echo "/dev/md0 exists. local ssd has been init"
        mkdir /localssd
        mount -o discard,defaults,nobarrier /dev/md0 /localssd
        return
    else
        echo '/dev/md0 not exist, begin to init local ssd'
    fi

    localssd_num=$(ls -al /dev | grep nvme0n | wc -l)
    echo localssd num: $localssd_num

    str_nvme0n=''
    for i in $(seq 1 $localssd_num); do
        str_nvme0n="$str_nvme0n /dev/nvme0n$i"
    done

    mdadm --create /dev/md0 --level=0 --raid-devices=$localssd_num $str_nvme0n

    mkfs.ext4 -F /dev/md0

    mv /etc/mdadm/mdadm.conf /etc/mdadm/mdadm.conf.bak
    sed -e '/\/dev\/md0/d' /etc/mdadm/mdadm.conf.bak >/etc/mdadm/mdadm.conf
    mdadm --detail --scan | tee -a /etc/mdadm/mdadm.conf

    update-initramfs -u

    if [ -d '/localssd' ]; then
        echo 'remove old /localssd and create new one'
        rm -rf /localssd
    fi

    mkdir /localssd
    mount -o discard,defaults,nobarrier /dev/md0 /localssd

    mount_result=$(df -h | grep /localssd)
    if [[ -z $mount_result ]]; then
        echo 'fail to mount /dev/md0 to /localssd'
        return
    fi

    mkdir -p /localssd/cache/tmp
    echo 'healthcheck' >/localssd/index.html

    mv /etc/fstab /etc/fstab.bak
    sed -e '/\/localssd/d' /etc/fstab.bak >/etc/fstab
    echo UUID=$(sudo blkid -s UUID -o value /dev/md0) /localssd ext4 discard,defaults,nobarrier,nofail 0 2 | tee -a /etc/fstab
}

update_cron() {
    #cronjob has not the environment variable
    ln -sf /snap/bin/gcloud /usr/bin/gcloud
    mv /etc/crontab /etc/crontab.bak
    sed -e '/update_nginx_proxy.sh/d' /etc/crontab.bak >/etc/crontab
    echo "* * * * * root bash /mnt/gcs/update_nginx_proxy.sh" >>/etc/crontab
    service cron restart
}

#init local ssd, mount, and 
nginx_type=$(curl -s 'http://metadata.google.internal/computeMetadata/v1/instance/attributes/nginx-proxy-type' -H 'Metadata-Flavor: Google')
nginx_conf_localssd=nginx_localssd.conf
nginx_conf_bypass=nginx_bypass.conf
source /mnt/gcs/global_conf

if [ $nginx_type = 'localssd' ]; then
    echo 'nginx proxy type: localssd'
    ln -sf /mnt/gcs/$nginx_conf_localssd /etc/nginx/nginx.conf
    echo $nginx_conf_localssd > /etc/nginx/current_nginx_conf
    init_local_ssd
    bash /mnt/gcs/update_upstream.sh
    service nginx restart
    echo "waiting for the MIG stable to update upstream conf again"
    gcloud compute instance-groups managed wait-until $mig_name --stable
    bash /mnt/gcs/update_upstream.sh
    service nginx restart
    update_cron
else
    echo 'nginx proxy type: bypass'
    ln -sf /mnt/gcs/$nginx_conf_bypass /etc/nginx/nginx.conf
    echo $nginx_conf_bypass > /etc/nginx/current_nginx_conf
    service nginx restart
fi
