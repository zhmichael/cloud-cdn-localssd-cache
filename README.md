## TL;DR
This is a automatic deployement solution of Google Cloud CDN layer 2 cache. In this solution, will use Nginx as the cache server, refer to this link:
https://www.nginx.com/blog/shared-caches-nginx-plus-cache-clusters-part-1/
<img src="image/nginx_cache.jpg" width=100%>

## Architecture
<img src="image/arch.jpg" width=75%>

## Feature Supported:
### Automatic deployment
- 1 command line to provision: cache size (Local SSD amount), instance type, instance amount, region, zone.
- Deploy Nginx proxy MIG with Linux Shell + GCP command line.
- Startup script in the image initializes it during boot.
### High availability, auto-recovery
- Use stateful MIG, once the VM broken the new VM will be launched with the same internal IP, the Layer 2 cache only lost 1/N
### Automatic cache size increase/decrease by 1-click
- Change instance amount in MIG to modify cache size.
- Cron job in Nginx proxy detects change, refreshes IP address list in 
Nginx config.
### Central configuration management 
- Cron job in Nginx proxy detects and reloads Nginx configuration file change for all the instances.
### Monitoring
- Use MQL to monitor cache rate via real back-origin egress network traffic, not from cache hit of Cloud CDN
### Logging
- Configuration change notification in Cloud Logging (VM change, Nginx configuration change)


## Automatic Deployement

1. Contact Michael to copy the Nginx image to your project
<img src="image/nginx_image.jpg" width=50%>

2. Create a configuraiton bucket for uploading the config files
<img src="image/bucket_1.jpg" width=50%>

3. Build the nginx configuration file for Local SSD proxy and Bypass proxy, and test them in your test environment.
<img src="image/nginx_conf.jpg" width=50%>

4. Config the global_conf file, set nginx_conf_localssd and nginx_conf_bypass paramenter. Suggest to leave stackdriver_logging=on
<img src="image/global_conf.jpg" width=50%>

5. Modify the paramneter in cdn-proxy-deploy.sh according to your deployement requirement.
<img src="image/deploy_file.jpg" width=50%>

| Parameter| Description |
| --- | ------ |
|     |        |
|     |        |
**PROJECT_ID** | You project id
**LOCALSSD_ZONE**| the loca ssd nginx proxy deploy zone 
**BYPASS_PROXY_REGION**| the failover nginx proxy deploy region
**CONFIG_BUCKET_NAME**| the configuration bucket name you create in step 2, ensure the right bucket name or Nginx will cannot find the conf file then health check failure
**IMAGE_NAME**| Image name you copied in step 1.
**LOCALSSD_MACHINE_TYPE**| Local SSD VM instance type
**BYPASS_MACHINE_TYPE**| Bypass VM instance type
**LOCALSSD_INSTANCE_NUM**| the number of Local SSD VM
**LOCALSSD_PER_INSTANCE**| The number of Local SSD disks in a Local SSD VM. 375GB per Local SSD. If you need 6TB, the quantity is 16; 9TB, the quantity is 24

6. Run "bash cdn-proxy-deploy.sh create" and wait for several minuts until the deployment is finished.
<img src="image/deploy1.jpg" width=75%>
<img src="image/deploy2.jpg" width=75%>

7. You will see these resources which be created

- Files uploaded to configuraiton bucket:
<img src="image/bucket_2.jpg" width=75%>

- Local SSD Nginx Proxy MIG:
<img src="image/MIG.jpg" width=75%>

- HTTP/HTTPS Load Balancer + CDN + MIG Backend Service:
<img src="image/LB.jpg" width=75%>

- Dashboard
<img src="image/dashboard.jpg" width=75%>

## Nginx Conf File Change Management
- The configuration GCS bucket is mounted to /mnt/gcs folder in each VM by using GCS Fuse
- The conf file nginx_localssd_xxx.conf  in GCS bucket is soft link to /etc/nginx.conf
- The cron job will run every minute to check if /mnt/gcs/global_conf is changed. If changed, will reload the new nginx conf file 
- Before the reload, the cron job will use nginx -t to check if the conf file is valid
- When the new conf file is reloaded or failed, the result will be displayed in cloud logging.
<img src="image/nginx_conf_central.jpg" width=100%>

## High Availability and Auto Healing
Because this deployment is using MIG, the VM will be automatically recoverd if down
## Cache Size expansion
Modify the VM size of the MIG to increase the cache size, the Nginx conf file will be updated automatically.
## Cache Size Shrink
Delete the VM from the MIG to decrease the cache size, the Nginx conf file will be updated automatically. You should delete VMs via internal IP address descending.
## Clean 
Run "bash cdn-proxy-deploy.sh clean" can clean the entire environment
<img src="image/clean.jpg" width=75%>
