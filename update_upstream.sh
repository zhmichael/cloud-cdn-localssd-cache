#!/bin/bash

update_upstream_conf() {
    UPSTREAM_FILE_NAME=/etc/nginx/upstream.conf
    PROJECT_ID=$(curl -s "http://metadata.google.internal/computeMetadata/v1/project/project-id" -H "Metadata-Flavor: Google")

    is_running=$(service nginx status | grep running)
    if [ "$is_running" = "" ]; then
        echo "start nginx service because it is not running"
        service nginx start
    fi

    echo "check ip address list in instance group"

    mig_name=$(curl -s 'http://metadata.google.internal/computeMetadata/v1/instance/attributes/created-by' -H 'Metadata-Flavor: Google')
    vm_num=$(gcloud compute instance-groups managed list-instances $mig_name | grep -wv NAME | wc -l)
    echo "MIG name: $mig_name, instance quantity: $vm_num"

    gcloud compute instances list --project=$PROJECT_ID | grep nginx-localssd-proxy- | awk -F' ' '{print $4}' >/tmp/ipaddr.list

    result=$?
    if [ "$result" != "0" ]; then
        echo 'gcloud compute instances list failed!'
        return -1
    fi

    new_ipaddr=$(sort -n -t . -k 1,1 -k 2,2 -k 3,3 -k 4,4 /tmp/ipaddr.list | xargs | sed 's/ *$//g')
    echo 'current ip list: '$new_ipaddr

    if [ "$new_ipaddr" = "" ]; then
        echo 'Fail to get the VM ip list!'
        return -1
    fi

    old_ipaddr=$(cat /etc/nginx/nginx_proxy_list)
    # old_ipaddr=''
    echo 'old ip list: '$old_ipaddr

    if [ "$new_ipaddr" != "$old_ipaddr" ]; then
        echo "nginx proxy server changed, old ip:$old_ipaddr, new ip:$new_ipaddr"
        echo 'upstream cache {' >$UPSTREAM_FILE_NAME
        echo $'\thash $uri consistent;' >>$UPSTREAM_FILE_NAME
        echo $'\tkeepalive 256;' >>$UPSTREAM_FILE_NAME
        echo $'\tkeepalive_timeout 600s;' >>$UPSTREAM_FILE_NAME
        echo $'\tkeepalive_requests 3500;' >>$UPSTREAM_FILE_NAME

        for ip in $new_ipaddr; do
            echo "found ip address in MIG: $ip"
            echo $'\tserver'" $ip:8080 max_fails=10 fail_timeout=10s;" >>$UPSTREAM_FILE_NAME
        done

        echo $'\tcheck port=8081 interval=5000 rise=2 fall=5 timeout=5000 type=http;' >>$UPSTREAM_FILE_NAME
        echo $'\tcheck_http_send "HEAD / HTTP/1.0\\r\\n\\r\\n";' >>$UPSTREAM_FILE_NAME
        echo $'\tcheck_http_expect_alive http_2xx http_3xx;' >>$UPSTREAM_FILE_NAME
        echo '}' >>$UPSTREAM_FILE_NAME
        echo $new_ipaddr >/etc/nginx/nginx_proxy_list

        #save the upstream.conf to gcs for check
        if [ ! -d "/mnt/gcs/upstream/" ]; then
            mkdir "/mnt/gcs/upstream/"
        fi
        cp $UPSTREAM_FILE_NAME "/mnt/gcs/upstream/"$(hostname)_upstream.conf
        cat $UPSTREAM_FILE_NAME
        str_log=$(hostname)": nginx upstream updated: "$(cat $UPSTREAM_FILE_NAME)
        gcloud logging write nginx-conf "$str_log" --project=$PROJECT_ID
        service nginx reload
    else
        echo "Nginx proxy server not changed, old ip:$old_ipaddr, new ip:$new_ipaddr"
    fi

    return 0
}

update_upstream_conf